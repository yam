#ifndef TRACK_PARAMETER_TYPE_H
#define TRACK_PARAMETER_TYPE_H

#include <vector>

namespace yam {
    using namespace std;

    class TrackParameterType {
        public:
            enum { BOOL, INT, FLOAT, STRING };              /* Datatypes */
            enum { LITERAL, PERCENT, DECIBEL };             /* Units     */
            enum { LINEAR, QUADRATIC, LOGARITHMIC, GAIN };  /* Scales    */ 
            enum { TOGGLE, OPTION, ADJUST, ENTER };         /* Controls  */

            /* General constructor */
            TrackParameterType(string name, int datatype, int unit, int scale, int control,
                void* defaultValue, void* minimumValue, void* maximumValue, vector<string>* options) :
                    name(name), datatype(datatype), unit(unit), scale(scale),
                    control(control), defaultValue(defaultValue), minimumValue(minimumValue),
                    maximumValue(maximumValue), options(options) { }

            /* Construct boolean toggle control */
            TrackParameterType(string name, bool defaultValue) :
                    name(name), datatype(BOOL), unit(0), scale(0),
                    control(TOGGLE), defaultValue(new bool(defaultValue)),
                    minimumValue(NULL), maximumValue(NULL), options(NULL) { }

            /* Construct boolean option control with two named options */
            TrackParameterType(string name, bool defaultValue, string falseString, string trueString) : 
                    name(name), datatype(BOOL), unit(0), scale(0), control(OPTION),
                    defaultValue(new bool(defaultValue)), minimumValue(NULL), maximumValue(NULL),
                    options(new vector<string>()) {
                this->options->push_back(falseString);
                this->options->push_back(trueString);
            }
           
            /* Construct integer adjust control */
            TrackParameterType(string name, int unit, int scale, int defaultValue, int minimumValue, int maximumValue) :
                    name(name), datatype(INT), unit(unit), scale(scale), control(ADJUST),
                    defaultValue(new int(defaultValue)), minimumValue(new int(minimumValue)),
                    maximumValue(new int(maximumValue)), options(NULL) { }

            /* Construct integer option control with maximumValue-minimumValue+1 named options */
            TrackParameterType(string name, int defaultValue, int minimumValue, int maximumValue, vector<string>* options) :
                    name(name), datatype(INT), unit(0), scale(0), control(OPTION),
                    defaultValue(new int(defaultValue)), minimumValue(new int(minimumValue)),
                    maximumValue(new int(maximumValue)), options(options) { }

            /* Construct float adjust control */
            TrackParameterType(string name, int unit, int scale, float defaultValue, float minimumValue, float maximumValue) :
                    name(name), datatype(FLOAT), unit(unit), scale(scale), control(ADJUST),
                    defaultValue(new float(defaultValue)), minimumValue(new float(minimumValue)),
                    maximumValue(new float(maximumValue)), options(NULL) { }

            /* Construct string entry control */
            TrackParameterType(string name, string defaultValue) :
                    name(name), datatype(STRING), unit(0), scale(0), control(ENTER),
                    defaultValue(new string(defaultValue)), minimumValue(NULL), maximumValue(NULL) { }

            string getName() { return name; }
            int getDatatype() { return datatype; }
            int getUnit() { return unit; }
            int getScale() { return scale; }
            int getControl() { return control; }
            void* getDefaultValue() { return defaultValue; }
            void* getMinimumValue() { return minimumValue; }
            void* getMaximumValue() { return maximumValue; }
            vector<string>* getOptions() { return options; }

        private:
            string name;
            int datatype;
            int unit;
            int scale;
            int control;
            void *defaultValue;
            void *minimumValue;
            void *maximumValue;
            vector<string>* options;
    };
};

#endif
