#ifndef TRACK_H
#define TRACK_H

#include <string>
#include <vector>
#include "TrackParameter.h"
#include "Region.h"

namespace yam {
    using namespace std;

    class Track {
        public:
            /* Track name */
            virtual string getName() = 0;
            virtual void setName(string name) = 0;

            /* Track parameters */
            virtual vector<TrackParameter*>* getTrackParameters() = 0;
            
            /* Regions in track */
            virtual vector<Region*>* getRegions() = 0;
            
            /* Sub-tracks */
            virtual vector<Track*>* getChildren() = 0;
    };
}

#endif
