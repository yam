#ifndef AUDIO_TRACK_H
#define AUDIO_TRACK_H

#include "Track.h"

namespace yam {
    class AudioTrack : public Track {
        public:    
            AudioTrack();
            virtual ~AudioTrack();

            string getName();
            void setName(string name);
            vector<TrackParameter*>* getTrackParameters();
            vector<Region*>* getRegions();
            vector<Track*>* getChildren();
        private:
            string name;
            vector<TrackParameter*>* trackParameters;
            static vector<TrackParameterType*>* trackParameterTypes;
    };
}

#endif
