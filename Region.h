#ifndef REGION_H
#define REGION_H

#include "Time.h"

namespace yam {
    class Region {
        public:
            virtual Time* getStartTime() = 0;
            virtual Time* getDuration() = 0;
            virtual bool isMuted() = 0;
    };
}

#endif
