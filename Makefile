CFLAGS=`pkg-config gtkmm-2.4 --cflags`
LIBS=`pkg-config gtkmm-2.4 --libs`

all: yam

yam: Yam.o AudioTrack.o
	g++ -o yam *.o $(LIBS)

%.o: %.cpp %.h *.h
	g++ -c -Wall -o $@ $< $(CFLAGS)

clean :
	rm -f yam *.o *~
