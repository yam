#include "AudioTrack.h"

using namespace yam;

vector<TrackParameterType*>* AudioTrack::trackParameterTypes = NULL;

AudioTrack::AudioTrack() : trackParameters(NULL) {

}

AudioTrack::~AudioTrack() {
}

string AudioTrack::getName() {
    return name;
}

void AudioTrack::setName(string name) {
    this->name = name;
}

vector<TrackParameter*>* AudioTrack::getTrackParameters() {
    if(trackParameters == NULL) {
        trackParameters = new vector<TrackParameter*>;
        
        if(trackParameterTypes == NULL) {
            trackParameterTypes = new vector<TrackParameterType*>;
            
            trackParameterTypes->push_back(new TrackParameterType("Volume",
                TrackParameterType::DECIBEL, TrackParameterType::GAIN, 1.f, 0.f, 2.f));

            trackParameterTypes->push_back(new TrackParameterType("Balance",
                TrackParameterType::PERCENT, TrackParameterType::LINEAR, 0.f, -1.f, 1.f));

            trackParameterTypes->push_back(new TrackParameterType("Trim",
                TrackParameterType::DECIBEL, TrackParameterType::GAIN, 1.f, 0.0625f, 16.f));
        }
    
        for(vector<TrackParameterType*>::iterator i = trackParameterTypes->begin(); i != trackParameterTypes->end(); i++) {
            trackParameters->push_back(new TrackParameter(*i));
        }
    }

    return trackParameters;
}

vector<Region*>* AudioTrack::getRegions() {
    return NULL;
}

vector<Track*>* AudioTrack::getChildren() {
    return NULL;
}

