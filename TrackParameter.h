#ifndef TRACK_PARAMETER_H
#define TRACK_PARAMETER_H

#include "TrackParameterType.h"

namespace yam {
    class TrackParameter {
        public:
            /* Construct track parameter from track type */
            TrackParameter(TrackParameterType* type) :
                    type(type), value(type->getDefaultValue()) { }
            
            TrackParameterType* getType() { return type; }
            void* getValue() { return value; }

        private:
            TrackParameterType* type;
            void* value;
    };
}

#endif
